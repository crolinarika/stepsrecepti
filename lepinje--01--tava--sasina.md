# lepinje iz tave sasina  
![](images/2023-05-01-02-01-22.png)

![](images/2023-05-01-02-01-49.png)

300 grama glatkog brasna  
![](images/2023-05-01-02-02-00.png)
![](images/2023-05-01-02-02-26.png)

5g soli  
![](images/2023-05-01-02-02-55.png)

1.5dl mineralne vode  
![](images/2023-05-01-02-03-19.png)
(moze i obicna voda)  
![](images/2023-05-01-02-03-38.png)

20g kiselog vrhnja ili jogurta  
![](images/2023-05-01-02-04-07.png)

50ml (pola decilitra) ulja  
![](images/2023-05-01-02-04-42.png)

[u jogurt/vrhnje uliti ulje]  
![](images/2023-05-01-02-05-04.png)

[dodati pola brasna]  
![](images/2023-05-01-02-05-20.png)

[mijesati]      
![](images/2023-05-01-02-05-39.png)

[dodati ostalu polovicu brasna]  
![](images/2023-05-01-02-05-58.png)

[tijesto mora biti malo ljepljivo]  
![](images/2023-05-01-02-07-13.png)

[promijesiti u rukama par minuta]  
![](images/2023-05-01-02-07-34.png)

[razrezati na 4 dijela]  
![](images/2023-05-01-02-08-05.png)

[razvaljati]  
![](images/2023-05-01-02-08-37.png)
![](images/2023-05-01-02-08-49.png)

[razrezati]  
![](images/2023-05-01-02-09-17.png)

[tava zagrijati na 8/9]  
![](images/2023-05-01-02-09-37.png)

[jako malo ulja s kistom]  
![](images/2023-05-01-02-10-19.png)

[4 min peci na jednu stranu zatvoreno]  
![](images/2023-05-01-02-10-41.png)

[3 min peci na drugu stranu zatvoreno]  
![](images/2023-05-01-02-11-42.png)
![](images/2023-05-01-02-12-14.png)
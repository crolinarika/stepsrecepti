# pierogi reciPeci
![](images/2023-04-30-23-05-02.png)
moze i sa tijestom za knedle sa krumpirom - recept za tijesto u jastucicima sa pekmezom
![](images/2023-04-30-23-43-38.png)


350g glatko brasno  
pola zlicice soli  
160ml tople vode  
jedno jaje
![](images/2023-04-30-22-59-48.png)

mijesiti  
![](images/2023-04-30-23-01-52.png)

staviti u krpu  
![](images/2023-04-30-23-02-27.png)

nadjev:  
1 cijela glavica crveni luk  
![](images/2023-04-30-23-03-21.png)
zlicica crvene slatke paprike u luk  
![](images/2023-04-30-23-15-49.png)
4 srednja krumpira  
malo posoliti i zgnjeciti u pire  
![](images/2023-04-30-23-04-00.png)
![](images/2023-04-30-23-12-37.png)

pomijesati pire i dinstani luk  
![](images/2023-04-30-23-37-25.png)
sir ili sirni namaz 70-100g   
![](images/2023-04-30-23-38-09.png)

malo papra  
![](images/2023-04-30-23-39-06.png)

tacnu namazati s brasnom da se ne lijepi  
![](images/2023-04-30-23-39-59.png)

tijesto 3-4mm debljine  
![](images/2023-04-30-23-40-38.png)
rezati  
![](images/2023-04-30-23-41-29.png)

nadjev u sredinu 1 zlica   
![](images/2023-04-30-23-41-59.png)
poklopiti i sa vilicom zatvoriti  
![](images/2023-04-30-23-42-32.png)

u vodu  
![](images/2023-04-30-23-44-22.png)

isprziti slaninu  
![](images/2023-04-30-23-44-53.png)
![](images/2023-04-30-23-45-16.png)

staviti pieroge u tavu gdje se przila slanina  
![](images/2023-04-30-23-45-50.png)
![](images/2023-04-30-23-46-13.png)

prziti s jedne i s druge strane - 1min za svaku stranu  
![](images/2023-04-30-23-47-10.png)

posluziti sa malo vrhnja  
![](images/2023-04-30-23-49-12.png)
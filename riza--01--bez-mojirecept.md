# riza moji recept
![](images/2023-05-01-19-27-14.png)

oprati da ne pusti sluz i ne zalijepi se  
![](images/2023-05-01-19-27-34.png)

2 zlicice masl. ulje ili ulje i maslac  
![](images/2023-05-01-19-28-20.png)

1 mjerica rize isprziti  
![](images/2023-05-01-19-28-40.png)

2 mjerice vruce vode politi  
![](images/2023-05-01-19-29-04.png)

promijesati  
![](images/2023-05-01-19-29-20.png)

1 zlicica soli  
![](images/2023-05-01-19-29-33.png)

na slaboj temp 2/6 10 minuta ostaviti kuhati poklopljeno  
![](images/2023-05-01-19-30-36.png)

jos 10 min ostaviti da stoji poslije  
![](images/2023-05-01-19-31-20.png)